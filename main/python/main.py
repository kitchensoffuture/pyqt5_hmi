# from fbs_runtime.application_context.PyQt5 import ApplicationContext
# from PyQt5.QtWidgets import QMainWindow
#
# import sys
#
# if __name__ == '__main__':
#     appctxt = ApplicationContext()       # 1. Instantiate ApplicationContext
#     window = QMainWindow()
#     window.resize(250, 150)
#     window.show()
#     exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
#     sys.exit(exit_code)
#

#!/usr/bin/env python


#############################################################################
##
## Copyright (C) 2013 Riverbank Computing Limited.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
#############################################################################

from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtCore import QDateTime, Qt, QTimer, QTime, pyqtSignal
from PyQt5.QtWidgets import (QApplication, QCheckBox, QComboBox, QDateTimeEdit,
        QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QProgressBar, QPushButton, QRadioButton, QScrollBar, QSizePolicy,
        QSlider, QSpinBox, QStyleFactory, QTableWidget, QTabWidget, QTextEdit,
        QVBoxLayout, QWidget)

import sys


class WidgetGallery(QDialog):
    switch_window = pyqtSignal(str)

    def __init__(self, parent=None):
        super(WidgetGallery, self).__init__(parent)

        self.originalPalette = QApplication.palette()

        styleComboBox = QComboBox()
        styleComboBox.addItems(QStyleFactory.keys())
        # print (QStyleFactory.keys()[0])

        styleLabel = QLabel("&Style:")
        styleLabel.setBuddy(styleComboBox)
        # styleComboBox = "macintosh"

        self.useStylePaletteCheckBox = QCheckBox("&Use style's standard palette")
        self.useStylePaletteCheckBox.setChecked(True)

        disableWidgetsCheckBox = QCheckBox("&Disable widgets")

        self.createTopLeftGroupBox()
        # self.createTopRightGroupBox()
        self.createBottomLeftTabWidget()
        self.createBottomRightGroupBox()
        self.createProgressBar()

        styleComboBox.activated[str].connect(self.changeStyle)
        self.useStylePaletteCheckBox.toggled.connect(self.changePalette)
        disableWidgetsCheckBox.toggled.connect(self.topLeftGroupBox.setDisabled)
        # disableWidgetsCheckBox.toggled.connect(self.topRightGroupBox.setDisabled)
        disableWidgetsCheckBox.toggled.connect(self.bottomLeftTabWidget.setDisabled)
        disableWidgetsCheckBox.toggled.connect(self.bottomRightGroupBox.setDisabled)

        topLayout = QHBoxLayout()
        topLayout.addWidget(styleLabel)
        topLayout.addWidget(styleComboBox)
        topLayout.addStretch(1)
        topLayout.addWidget(self.useStylePaletteCheckBox)
        topLayout.addWidget(disableWidgetsCheckBox)

        mainLayout = QGridLayout()
        mainLayout.addLayout(topLayout, 0, 0, 1, 2)
        mainLayout.addWidget(self.topLeftGroupBox, 1, 0)
        # mainLayout.addWidget(self.topRightGroupBox, 1, 1)
        mainLayout.addWidget(self.bottomLeftTabWidget, 2, 0)
        mainLayout.addWidget(self.bottomRightGroupBox, 2, 1)
        mainLayout.addWidget(self.progressBar, 3, 0, 1, 2)
        mainLayout.setRowStretch(1, 1)
        mainLayout.setRowStretch(2, 1)
        mainLayout.setColumnStretch(0, 1)
        mainLayout.setColumnStretch(1, 1)
        self.setLayout(mainLayout)

        self.setWindowTitle("Smart Kitchen")
        self.changeStyle('macintosh')

    def changeStyle(self, styleName):
        QApplication.setStyle(QStyleFactory.create(styleName))
        self.changePalette()

    def changePalette(self):
        if (self.useStylePaletteCheckBox.isChecked()):
            QApplication.setPalette(QApplication.style().standardPalette())
        else:
            QApplication.setPalette(self.originalPalette)

    def advanceProgressBar(self):
        curVal = self.progressBar.value()
        maxVal = self.progressBar.maximum()
        self.progressBar.setValue(curVal + (maxVal - curVal) / 100)

    def createTopLeftGroupBox(self):
        self.topLeftGroupBox = QGroupBox("Machine Process")

        # radioButton1 = QRadioButton("Radio button 1")
        # radioButton2 = QRadioButton("Radio button 2")
        # radioButton3 = QRadioButton("Radio button 3")
        # radioButton1.setChecked(True)
        #
        # checkBox = QCheckBox("Tri-state check box")
        # checkBox.setTristate(True)
        # checkBox.setCheckState(Qt.PartiallyChecked)
        processLabel = QLabel("Process No:")
        spinBoxNo = QSpinBox(self.topLeftGroupBox)
        spinBoxNo.setValue(50)
        processComboBox = QComboBox()
        processComboBox.addItems(['Heating', 'Dispensing'])

        processType = QLabel("Process Type:")
        processIngredient = QLabel("Ingredient:")
        textEditIngredient = QLineEdit()
        textEditIngredient.setText("Spices")

        processQuantity = QLabel("Quantity:")
        spinBoxQty = QSpinBox(self.topLeftGroupBox)
        spinBoxQty.setValue(5)

        # layout = QVBoxLayout()
        layout = QGridLayout()
        layout.addWidget(processLabel, 1, 0)
        layout.addWidget(spinBoxNo, 1, 1)
        layout.addWidget(processType, 2, 0)
        layout.addWidget(processComboBox, 2, 1)
        layout.addWidget(processIngredient)
        layout.addWidget(textEditIngredient)
        layout.addWidget(processQuantity)
        layout.addWidget(spinBoxQty)
        # layout.addStretch(1)
        self.topLeftGroupBox.setLayout(layout)

    def createTopRightGroupBox(self):
        self.topRightGroupBox = QGroupBox("Group 2")

        defaultPushButton = QPushButton("Empty")
        defaultPushButton.setDefault(True)
        defaultPushButton.setCheckable(True)

        togglePushButton = QPushButton("Empty")
        togglePushButton.setCheckable(True)
        togglePushButton.setChecked(True)

        #
        # flatPushButton = QPushButton("Flat Push Button")
        # flatPushButton.setFlat(True)

        layout = QVBoxLayout()
        layout.addWidget(defaultPushButton)
        layout.addWidget(togglePushButton)
        # layout.addWidget(flatPushButton)
        layout.addStretch(1)
        self.topRightGroupBox.setLayout(layout)

    def createBottomLeftTabWidget(self):
        self.bottomLeftTabWidget = QGroupBox("Interact with Machine")

        defaultPushButton = QPushButton("Stop Machine")
        defaultPushButton.setDefault(True)
        defaultPushButton.setCheckable(True)

        togglePushButton = QPushButton("Start Magic")
        togglePushButton.setCheckable(True)
        togglePushButton.setChecked(True)
        togglePushButton.clicked.connect(self.switch)

        #
        # flatPushButton = QPushButton("Flat Push Button")
        # flatPushButton.setFlat(True)

        layout = QVBoxLayout()
        layout.addWidget(defaultPushButton)
        layout.addWidget(togglePushButton)
        # layout.addWidget(flatPushButton)
        layout.addStretch(1)
        self.bottomLeftTabWidget.setLayout(layout)

        # self.bottomLeftTabWidget = QTabWidget()
        # self.bottomLeftTabWidget.setSizePolicy(QSizePolicy.Preferred,
        #         QSizePolicy.Ignored)
        #
        # tab1 = QWidget()
        # tableWidget = QTableWidget(10, 10)
        #
        # tab1hbox = QHBoxLayout()
        # tab1hbox.setContentsMargins(5, 5, 5, 5)
        # tab1hbox.addWidget(tableWidget)
        # tab1.setLayout(tab1hbox)
        #
        # tab2 = QWidget()
        # textEdit = QTextEdit()
        #
        # textEdit.setPlainText("Twinkle, twinkle, little star,\n"
        #                       "How I wonder what you are.\n"
        #                       "Up above the world so high,\n"
        #                       "Like a diamond in the sky.\n"
        #                       "Twinkle, twinkle, little star,\n"
        #                       "How I wonder what you are!\n")
        #
        # tab2hbox = QHBoxLayout()
        # tab2hbox.setContentsMargins(5, 5, 5, 5)
        # tab2hbox.addWidget(textEdit)
        # tab2.setLayout(tab2hbox)
        #
        # self.bottomLeftTabWidget.addTab(tab1, "&Table")
        # self.bottomLeftTabWidget.addTab(tab2, "Text &Edit")

    def createBottomRightGroupBox(self):
        self.bottomRightGroupBox = QGroupBox("Estimated time")
        self.bottomRightGroupBox.setCheckable(True)
        self.bottomRightGroupBox.setChecked(True)

        # lineEdit = QLineEdit('s3cRe7')
        # lineEdit.setEchoMode(QLineEdit.Password)
        #
        # spinBox = QSpinBox(self.bottomRightGroupBox)
        # spinBox.setValue(50)
        timeRemaining = QLabel("Estimated Time:")
        # dateTimeEdit = QTime.currentTime();
        # dateTimeEdit.setTime(QTime.currentTime())
        dateTimeEdit = QDateTimeEdit(self.bottomRightGroupBox)
        dateTimeEdit.setDateTime(QDateTime.currentDateTime())
        # slider = QSlider(Qt.Horizontal, self.bottomRightGroupBox)
        # slider.setValue(40)
        #
        # scrollBar = QScrollBar(Qt.Horizontal, self.bottomRightGroupBox)
        # scrollBar.setValue(60)
        #
        # dial = QDial(self.bottomRightGroupBox)
        # dial.setValue(30)
        # dial.setNotchesVisible(True)

        layout = QGridLayout()
        # layout.addWidget(lineEdit, 0, 0, 1, 2)
        # layout.addWidget(spinBox, 1, 0, 1, 2)
        layout.addWidget(timeRemaining)
        layout.addWidget(dateTimeEdit)
        # layout.addWidget(slider, 3, 0)
        # layout.addWidget(scrollBar, 4, 0)
        # layout.addWidget(dial, 3, 1, 2, 1)
        # layout.setRowStretch(5, 1)
        self.bottomRightGroupBox.setLayout(layout)

    def createProgressBar(self):
        self.progressBar = QProgressBar()
        self.progressBar.setRange(0, 10000)
        self.progressBar.setValue(0)

        timer = QTimer(self)
        timer.timeout.connect(self.advanceProgressBar)
        timer.start(1000)

    def switch(self):
        self.switch_window.emit('window_two')


class WindowTwo(QWidget):
    switch_window = pyqtSignal(str)
    def __init__(self, text):
        QWidget.__init__(self)
        self.setWindowTitle('Smart Kitchen')

        layout = QGridLayout()

        self.label = QLabel('Select a dish to cook!')
        layout.addWidget(self.label)
        dishSelectionComboBox = QComboBox()
        dishSelectionComboBox.addItems(['Paneer Lababdar', 'Dal Makhani'])

        self.button = QPushButton('Make it!')
        self.button.clicked.connect(self.switch)
        layout.addWidget(dishSelectionComboBox)
        layout.addWidget(self.button)

        self.setLayout(layout)

    def switch(self):
        self.switch_window.emit('widget_gallery')


class Controller:

    def __init__(self):
        pass

    def show_widget_gallery(self):
        self.widget_gallery = WidgetGallery()
        self.widget_gallery.switch_window.connect(self.show_window_two)
        self.window_two.close()
        self.widget_gallery.show()

    def show_window_two(self, text):
        self.window_two = WindowTwo(text)
        self.window_two.switch_window.connect(self.show_widget_gallery)
        # self.widget_gallery.close()
        self.window_two.show()


def main():
    app = QApplication(sys.argv)
    controller = Controller()
    controller.show_window_two('Show')
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
    # appctxt = ApplicationContext()
    # gallery = WidgetGallery()
    # gallery.show()
    # sys.exit(appctxt.app.exec_())
